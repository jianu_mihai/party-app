export enum TodoStatuses {
  pending = "pending",
  done = "done",
}

export interface TodoItem {
  id: string;
  title: string;
  status: TodoStatuses;
  mode: string;
}

export type TodosById = { [id: string]: TodoItem };
