import "todomvc-app-css/index.css";
import Todo from "./Todo";
import Footer from "./Footer";

const App: view = ({
  todoIds = observe.visibleTodoIds,
  todosById = observe.todosById,
}) => (
  <section className="todoapp">
    {console.log(todosById)}
    <header className="header">
      <h1>todos</h1>
    </header>
    <input
      className="new-todo"
      placeholder="What needs to be done?"
      autoFocus={true}
    />
    <section className="main">
      <input id="toggle-all" className="toggle-all" type="checkbox" />
      <label htmlFor="toggle-all">Mark all as complete</label>

      <ul className="todo-list">
        {todoIds.map((id: string) => (
          <Todo id={id} key={id} />
        ))}
      </ul>
    </section>
    <Footer />;
  </section>
);

export default App;
