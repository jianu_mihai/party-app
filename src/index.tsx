import { engine } from "@c11/engine.runtime";
import { render } from "@c11/engine.react";
import App from "./App";
import "./global";

const app = engine({
  state: {
    todosById: {
      todo1: {
        id: "todo1",
        title: "Add initial state to engine",
        status: "pending",
        mode: "viewing",
      },
      todo2: {
        id: "todo2",
        title: "Use initial state in components",
        status: "done",
        mode: "viewing",
      },
      todo3: {
        id: "todo3",
        title: "Update state in components",
        status: "pending",
        mode: "editing",
      },
    },

    visibleTodoIds: ["todo1", "todo2"],
  },

  use: [render(<App />, "#app", {})],
});

app.start();

// Each renderer can have a state where it stores if it finished rendering/mounting/etc
// This can be used to hook-up processes for export for example
