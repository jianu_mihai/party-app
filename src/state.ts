import type { TodosById } from "./types";

export type State = {
  todosById: TodosById;
  visibleTodoIds: string[];
};
