import { TodoStatuses } from "./types";

const Todo: view = ({
  title = observe.todosById[prop.id].title,
  status = observe.todosById[prop.id].status,
  updateStatus = update.todosById[prop.id].status,
}) => (
  <li>
    <div className="view">
      <input
        type="checkbox"
        className="toggle"
        checked={status === TodoStatuses.done}
        onChange={() =>
          updateStatus.set(
            status === TodoStatuses.done
              ? TodoStatuses.pending
              : TodoStatuses.done
          )
        }
      />
      <label>{title}</label>
      <button className="destroy" />
    </div>
  </li>
);
export default Todo;
