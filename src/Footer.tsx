import { TodosById } from "./types";
import { TodoStatuses } from "./types";
const Footer: view = () => (
  <footer className="footer">
    <span className="todo-count">
      <strong>{pendingCounter}</strong> items left
    </span>
    <ul className="filters">
      <li>
        <a href="#/" className="selected">
          All
        </a>
      </li>
      <li>
        <a href="#/active">Active</a>
      </li>
      <li>
        <a href="#/completed">Completed</a>
      </li>
    </ul>
    <button className="clear-completed">Clear completed</button>
  </footer>
);

const pendingCounter: producer = ({
  updatePendingCount = update.pendingCount,
  todosByid = observe.todosById,
}) => {
  const pendingCount = Object.values(todosByid as TodosById).reduce(
    (accum: number, todo) =>
      todo.status === TodoStatuses.done ? accum : accum + 1,
    0
  );

  updatePendingCount.set(pendingCount);
};

Footer.producers([pendingCounter]);
export default Footer;
